Release notes for ERPA App System v0.09.07
==========================================

This release is available at:
   https://gitlab.com/clickstart-public/erpa-public/tree/master/apk

It consists of the following files:
* This README file
* app-debug_v0.09.07.apk
* Faveo_Helpdesk_Community__1.8.1_.apk

Links:
* The URL of the experimental ERPA Helpdesk website (running on Faveo Helpdesk Community)
http://196.253.97.35:8081/


How to test this release:
=========================

A) Testing the ERPA Helpdesk (ticket system)
B) Testing the new ERPA App prototype


A) Testing the ERPA Helpdesk (ticket system)
--------------------------------------------
Step 1) 
* You can use a web browser on a PC, tablet or phone.
* Register as a user on the experimental ERPA Helpdesk website
      http://196.253.97.35:8081/
* Use a real e-mail address to receive a confirmation message and activate that account

Step 2) 
* Log in to the website with your e-mail address and chosen password.
* You will be registered as normal user that can sumbit tickets.

Step 3)
* Install the Faveo_Helpdesk_Community__1.8.1_.apk on Android

Step 4)
* Use the same account (created in step 1) with the Faveo_Helpdesk_Community App.
* Submit a ticket (this will be as a normal user)

Step 5)
* Log in as an admin-user on the website:
    Email: info@erpa.co.za
    Password: infoerpa
  You can use a browser on a PC, tablet or phone.
* As admin-user you have the option to switch to the "Agent Panel" (to handle tickets) or the "Admin Panel" to change system settings etc.  (Some of the customisation have been done here, such as uploading the ERPA logo.)
* Note: There may be an "Alert" message "You have not configured system mail." This only affects the functionality of the system to receive tickets via e-mail and we do not plan to support that.


B) Testing the new ERPA App prototype
-------------------------------------

Install:  app-debug_v0.09.07.apk (The new prototype of the ERPA App) 

What to look for in this prototype release:
...

Known limitations:
...
